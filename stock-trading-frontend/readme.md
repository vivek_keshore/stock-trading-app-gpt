## Stock Trading App

## Description

This project is a stock trading dashboard where users can view their account details, watch stock prices, and buy or sell stocks. It's built using Vue.js, a progressive JavaScript framework for building user interfaces. The dashboard includes features such as account details, a watchlist of stocks, and the ability to buy or sell stocks. It also provides user management options such as updating user details and password.

## Features

- User Authentication
- Watchlist of Stocks
- Account Details
- Buy/Sell Stocks
- Update User Profile and Password
- Installation
- Ensure you have Node.js and npm installed.

## Clone the repository

```bash
git clone https://github.com/username/projectname.git
```

## Navigate into the project directory

```bash
cd projectname
```

## Install dependencies

```bash
npm install
```

## Environment Variables

To run this project, you will need to add the following environment variables to your .env file

VUE_APP_API_URL - The backend API URL.

## Running the Project

```bash
# Serve with hot reload at localhost:8080
npm run serve

# Build for production with minification
npm run build
```

Navigate to http://localhost:5173 to view the project.
