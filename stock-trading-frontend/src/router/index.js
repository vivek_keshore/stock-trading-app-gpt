import { createRouter, createWebHistory } from "vue-router";
import SignIn from "../views/SignIn.vue";
import SignUp from "../views/SignUp.vue";
import OTPValidation from "../views/OTPValidation.vue";
import Dashboard from "../views/Dashboard.vue";
import HomePage from "../views/HomePage.vue";

const routes = [
  { path: "/", component: HomePage },
  { path: "/signin", component: SignIn },
  { path: "/signup", component: SignUp },
  { path: "/validate-otp", name: "validate-otp", component: OTPValidation },
  { path: "/dashboard", component: Dashboard },
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

export default router;
