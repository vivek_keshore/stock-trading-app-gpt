from flasgger import Swagger
from flask import Flask
from flask_pymongo import PyMongo
from app.mailers.mailer import mail
from flask_cors import CORS

from config import DevelopmentConfig

mongo = PyMongo()

swagger_template = {
	"info": {
		"title": "Stock Trading App APIs",
		"description": "Trade smarter, invest better. Your ultimate stock trading app for informed decisions and financial growth.",
		"version": "0.0.1",
		"contact": {
			"name": "SenecaGlobal",
			"email": "vivek.keshore@senecaglobal.com",
		},
	},
	"components": {
		"securitySchemes": {
			"bearerAuth": {"type": "http", "scheme": "bearer", "bearerFormat": "JWT"}
		}
	},
	"security": [{"bearerAuth": []}],
}


def create_app():
	app = Flask(__name__)
	app.config.from_object(DevelopmentConfig)

	mongo.init_app(app)
	mail.init_app(app)

	app.config["SWAGGER"] = {
		"title": "Stock Trading App APIs",
		"openapi": "3.0.2",
		"uiversion": 3,
		"specs_route": "/swagger/",
	}
	Swagger(app, template=swagger_template)
	CORS(app)

	from app.api.auth import auth_bp
	from app.api.dashboard import dashboard_bp
	from app.api.orders import order_bp
	from app.api.stocks import stocks_bp
	from app.api.user import user_bp

	app.register_blueprint(auth_bp)
	app.register_blueprint(dashboard_bp)
	app.register_blueprint(order_bp)
	app.register_blueprint(stocks_bp)
	app.register_blueprint(user_bp)

	return app
