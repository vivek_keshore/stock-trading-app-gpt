import random
import time

import jwt
from flask import Blueprint, request, current_app
from flask_mail import Message
from flasgger import swag_from

from app.mailers.mailer import mail
from app.models.user import User

auth_bp = Blueprint('auth', __name__)


def send_otp(email, otp):
    msg = Message('OTP for Login', recipients=[email])
    msg.body = f'Your OTP for login is {otp}'
    mail.send(msg)


@auth_bp.route('/signin', methods=['POST'])
@swag_from('/app/docs/user/signin.yml')
def signin():
    data = request.get_json()
    user = User.get_by_email(data['email'])
    if user and user.check_password(data['password']):
        otp = random.randint(100000, 999999)
        user.save_otp(otp)
        send_otp(data['email'], otp)
        return {'status': 'OTP sent'}
    return {'status': 'failure'}, 400


@auth_bp.route('/verify_otp', methods=['POST'])
@swag_from('/app/docs/user/validate_otp.yml')
def verify_otp():
    data = request.get_json()
    user = User.get_by_email(data['email'])
    if user and user.verify_otp(data['otp']):
        token = jwt.encode({'email': user.email, 'exp' : time.time() + 3600}, current_app.config['SECRET_KEY'])
        return {'token': token}
    return {'status': 'failure'}, 400
