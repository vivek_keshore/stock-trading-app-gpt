from flasgger import swag_from
from flask import Blueprint, g

from app.models.order import Order
from app.models.stock import Stock
from app.models.user import User
from app.token import token_required

dashboard_bp = Blueprint('dashboard', __name__)


@dashboard_bp.route('/dashboard', methods=['GET'])
@swag_from('/app/docs/dashboard/dashboard.yml')
@token_required
def dashboard():
    email = g.current_user.email
    user = User.get_by_email(email)
    if user:
        orders = Order.get_orders_by_user_id(user.get_id())
        stocks = Stock.get_all_stocks()
        return {'user': user, 'orders': orders, 'stocks': stocks}
    return {'status': 'failure'}


@dashboard_bp.route('/dashboard/info', methods=['GET'])
@swag_from('/app/docs/dashboard/dashboard_info.yml')
@token_required
def get_dashboard_info():
    email = g.current_user.email
    user = User.get_by_email(email)
    if user:
        return user.serialize_dashboard_info(), 200
    else:
        return {'message': 'User not found'}, 404


@dashboard_bp.route('/dashboard/watchlist', methods=['GET'])
@swag_from('/app/docs/dashboard/watchlist.yml')
@token_required
def get_watchlist():
    email = g.current_user.email
    user = User.get_by_email(email)
    if user:
        stocks = Stock.get_watchlist_by_user_id(user.id)
        return [stock.serialize() for stock in stocks], 200
    else:
        return {'message': 'User not found'}, 404

