from flasgger import swag_from
from flask import Blueprint, request

from app.models.order import Order
from app.token import token_required

order_bp = Blueprint('order_bp', __name__)


@order_bp.route('/order', methods=['POST'])
@swag_from('/app/docs/order/create.yml')
@token_required
def create_order():
	data = request.get_json()
	new_order = Order.create_order(
		data['user_id'], data['stock_id'], data['quantity'],
		data['order_type'], data['order_option'], data['price']
	)
	return new_order.serialize(), 201


@order_bp.route('/order/<order_id>', methods=['GET'])
@swag_from('/app/docs/order/get.yml')
@token_required
def get_order(order_id):
	order = Order.get_order(order_id)
	return order.serialize() if order else {'message': 'Order not found'}, 404


@order_bp.route('/order/user/<user_id>', methods=['GET'])
@swag_from('/app/docs/order/list_by_user.yml')
@token_required
def get_orders_by_user_id(user_id):
	orders = Order.get_orders_by_user_id(user_id)
	return [order.serialize() for order in orders], 200


@order_bp.route('/order/<order_id>', methods=['PUT'])
@swag_from('/app/docs/order/update.yml')
@token_required
def update_order(order_id):
	data = request.get_json()
	updated_order = Order.update_order(
		order_id, data.get('user_id'), data.get('stock_id'),
		data.get('quantity'), data.get('order_type'),
		data.get('order_option'), data.get('price')
	)
	return updated_order.serialize() if updated_order else {'message': 'Order not found'}, 404


@order_bp.route('/order/<order_id>', methods=['DELETE'])
@swag_from('/app/docs/order/delete.yml')
@token_required
def delete_order(order_id):
	if Order.delete_order(order_id):
		return {'message': 'Order deleted'}, 200
	else:
		return {'message': 'Order not found'}, 404
