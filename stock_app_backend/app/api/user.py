from flasgger import swag_from
from flask import Blueprint, request, jsonify, g
from werkzeug.security import generate_password_hash, check_password_hash

from app.models.user import User
from app.token import token_required

user_bp = Blueprint('user', __name__)


@user_bp.route('/signup', methods=['POST'])
@swag_from('/app/docs/user/signup.yml')
def create_user():
    data = request.get_json()
    try:
        user = User.create_user(data['email'], data['password'], data['name'], data.get('opening_balance', 0))
    except ValueError as e:
        return jsonify({'message': str(e), 'status': 'failure'}), 400

    return jsonify({
        'email': user.email,
        'name': user.name,
        'opening_balance': user.opening_balance,
        'status': 'success'
    }), 201


@user_bp.route('/get', methods=['GET'])
@swag_from('/app/docs/user/get.yml')
@token_required
def get_user_details():
    email = g.current_user.email
    user = User.get_by_email(email)
    if user:
        return {
            'name': user.name,
            'email': user.email,
            'opening_balance': user.opening_balance,
            'available_margin': user.available_margin,
            'margin_used': user.margin_used
        }
    return {'message': 'User not found'}, 404


@user_bp.route('/change_password', methods=['PUT'])
@swag_from('/app/docs/user/change_password.yml')
@token_required
def change_password():
    data = request.get_json()
    email = g.current_user.email
    old_password = data['old_password']
    new_password = data['new_password']

    user = User.get_by_email(email)
    if not user or not check_password_hash(user.password, old_password):
        return jsonify({'message': 'Invalid email or password', 'status': 'failure'}), 400

    user.password = generate_password_hash(new_password)
    User.get_collection().update_one({'email': email}, {'$set': {'password': user.password}})

    return jsonify({'message': 'Password updated successfully', 'status': 'success'})


@user_bp.route('/update_profile', methods=['PUT'])
@swag_from('/app/docs/user/update_profile.yml')
@token_required
def update_profile():
    data = request.get_json()
    email = g.current_user.email
    name = data.get('name')
    opening_balance = data.get('opening_balance')

    user = User.get_by_email(email)
    if not user:
        return jsonify({'message': 'User not found', 'status': 'failure'}), 400

    if name is not None:
        user.name = name
    if opening_balance is not None:
        user.opening_balance = opening_balance

    User.get_collection().update_one({'email': email}, {'$set': user.__dict__})

    return jsonify({'message': 'Profile updated successfully', 'status': 'success'})
