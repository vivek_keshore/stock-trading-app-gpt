from bson.objectid import ObjectId
from pymongo import MongoClient
from flask import current_app


class Order:
	def __init__(self, data):
		self.id = str(data['_id'])
		self.user_id = data['user_id']
		self.stock_id = data['stock_id']
		self.quantity = data['quantity']
		self.order_type = data['order_type']
		self.order_option = data['order_option']
		self.price = data['price']

	@staticmethod
	def get_collection():
		client = MongoClient(current_app.config['MONGO_URI'])
		db = client.get_database()
		return db['order']

	@staticmethod
	def create_order(user_id, stock_id, quantity, order_type, order_option, price):
		new_order = {
			'user_id': user_id, 'stock_id': stock_id, 'quantity': quantity,
			'order_type': order_type, 'order_option': order_option, 'price': price
		}
		order_collection = Order.get_collection()
		order_collection.insert_one(new_order)
		return Order(new_order)

	@staticmethod
	def get_order(order_id):
		order_collection = Order.get_collection()
		order_data = order_collection.find_one({'_id': ObjectId(order_id)})
		return Order(order_data) if order_data else None

	@staticmethod
	def get_all_orders():
		order_collection = Order.get_collection()
		orders_data = order_collection.find()
		return [Order(order_data) for order_data in orders_data]

	@staticmethod
	def update_order(order_id, user_id=None, stock_id=None, quantity=None,
					 order_type=None, order_option=None, price=None):
		update_fields = {}
		if user_id is not None:
			update_fields['user_id'] = user_id
		if stock_id is not None:
			update_fields['stock_id'] = stock_id
		if quantity is not None:
			update_fields['quantity'] = quantity
		if order_type is not None:
			update_fields['order_type'] = order_type
		if order_option is not None:
			update_fields['order_option'] = order_option
		if price is not None:
			update_fields['price'] = price
		order_collection = Order.get_collection()
		order_collection.update_one({'_id': ObjectId(order_id)}, {'$set': update_fields})
		return Order(order_collection.find_one({'_id': ObjectId(order_id)}))

	@staticmethod
	def delete_order(order_id):
		order_collection = Order.get_collection()
		order_data = order_collection.find_one({'_id': ObjectId(order_id)})
		if not order_data:
			return None
		order_collection.delete_one({'_id': ObjectId(order_id)})
		return True

	@staticmethod
	def get_orders_by_user_id(user_id):
		order_collection = Order.get_collection()
		orders = order_collection.find({"user_id": user_id})
		return [Order(order) for order in orders]

	def serialize(self):
		return {
			'id': self.id,
			'user_id': self.user_id,
			'stock_id': self.stock_id,
			'quantity': self.quantity,
			'order_type': self.order_type,
			'order_option': self.order_option,
			'price': self.price
		}
