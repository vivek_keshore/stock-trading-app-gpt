from bson.objectid import ObjectId
from flask import current_app
from pymongo import MongoClient


class Stock:
	def __init__(self, data):
		self.id = str(data['_id'])
		self.name = data['name']
		self.price = data['price']
		self.status = data['status']

	@staticmethod
	def get_collection():
		client = MongoClient(current_app.config['MONGO_URI'])
		db = client.get_database()
		return db['stock']

	@staticmethod
	def create_stock(name, price, status):
		stock_collection = Stock.get_collection()
		existing_stock = stock_collection.find_one({'name': name})
		if existing_stock:
			return None
		new_stock = {'name': name, 'price': price, 'status': status}
		stock_collection.insert_one(new_stock)
		return Stock(new_stock)

	@staticmethod
	def get_stock(stock_id):
		stock_collection = Stock.get_collection()
		stock_data = stock_collection.find_one({'_id': ObjectId(stock_id)})
		return Stock(stock_data) if stock_data else None

	@staticmethod
	def get_all_stocks():
		stock_collection = Stock.get_collection()
		stocks_data = stock_collection.find()
		return [Stock(stock_data) for stock_data in stocks_data]

	@staticmethod
	def get_watchlist_by_user_id(user_id):
		stock_collection = Stock.get_collection()
		stocks_data = stock_collection.find({'user_id': user_id})
		if stocks_data:
			return [Stock(data) for data in stocks_data]
		else:
			return []

	@staticmethod
	def update_stock(stock_id, name=None, price=None, status=None):
		stock_collection = Stock.get_collection()
		existing_stock = stock_collection.find_one({'name': name})
		if existing_stock and existing_stock['_id'] != ObjectId(stock_id):
			return None
		update_fields = {}
		if name is not None:
			update_fields['name'] = name
		if price is not None:
			update_fields['price'] = price
		if status is not None:
			update_fields['status'] = status
		stock_collection.update_one({'_id': ObjectId(stock_id)}, {'$set': update_fields})
		return Stock(stock_collection.find_one({'_id': ObjectId(stock_id)}))

	@staticmethod
	def delete_stock(stock_id):
		stock_collection = Stock.get_collection()
		stock_data = stock_collection.find_one({'_id': ObjectId(stock_id)})
		if not stock_data:
			return None
		stock_collection.delete_one({'_id': ObjectId(stock_id)})
		return True

	def serialize(self):
		return {
			'id': self.id,
			'name': self.name,
			'price': self.price,
			'status': self.status
		}
