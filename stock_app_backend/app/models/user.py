from flask import current_app
from pymongo import MongoClient
from werkzeug.security import generate_password_hash, check_password_hash


class User:
    def __init__(self, email, password, name=None, opening_balance=None, _id=None, otp=None):
        self.id = _id
        self.email = email
        self.password = password
        self.name = name
        self.opening_balance = opening_balance
        self.otp = otp

    @staticmethod
    def get_collection():
        client = MongoClient(current_app.config['MONGO_URI'])
        db = client.get_database()
        return db['user']

    @staticmethod
    def get_by_email(email):
        collection = User.get_collection()
        user_data = collection.find_one({'email': email})
        return User(**user_data) if user_data else None

    @classmethod
    def create_user(cls, email, password, name, opening_balance):
        user = cls.get_by_email(email)
        if user is not None:
            raise ValueError('A user with this email already exists')

        user = cls(email, generate_password_hash(password), name, opening_balance)
        collection = cls.get_collection()
        result = collection.insert_one(user.__dict__)
        if result.acknowledged:
            return user

    def set_password(self, password):
        self.password = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password, password)

    def save_otp(self, otp):
        collection = User.get_collection()
        collection.update_one({'email': self.email}, {'$set': {'otp': otp}})

    def verify_otp(self, otp):
        collection = User.get_collection()
        user_data = collection.find_one({'email': self.email})
        return str(user_data.get('otp')) == otp if user_data else None

    def serialize_dashboard_info(self):
        return {
            'name': self.name,
            'available_margin': 50000,
            'margin_used': 0,
            'opening_balance': self.opening_balance
        }
