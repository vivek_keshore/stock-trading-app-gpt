from functools import wraps

import jwt
from flask import request, jsonify, current_app as app, g

from app.models.user import User


def token_required(f):
	@wraps(f)
	def decorated(*args, **kwargs):
		token = None
		if 'x-access-token' in request.headers:
			token = request.headers['x-access-token']

		if not token:
			token = request.authorization.token

		if not token:
			return jsonify({'message': 'Token is missing.'}), 401

		try:
			data = jwt.decode(token, app.config['SECRET_KEY'], algorithms=["HS256"])
			g.current_user = User.get_by_email(data['email'])
		except Exception as ex:
			return jsonify({'message': 'Token is invalid.'}), 401

		return f(*args, **kwargs)

	return decorated
