# Stock Trading App

## Introduction

This is a stock trading web application built using Vue.js for the frontend and Flask for the backend. The application
supports basic user authentication functionalities such as sign up, sign in, and OTP validation. Once authenticated, the
user can view their dashboard which displays their info and a watchlist of predefined stocks. Users can also perform
CRUD operations for stocks and orders.

## Prerequisites

Before you begin, ensure you have met the following requirements:

- You have installed Python 3.6 or later.
- You have installed Pipenv.
- You have Docker installed (optional).
- You have Docker Compose installed (optional).

## Installing Stock Trading App

To install the Stock Trading App, follow these steps:

1. Clone the repository:

```bash
git clone https://github.com/your_username/stock-trading-app.git
cd stock-trading-app
```

2. Install the dependencies:

```bash
pipenv install
```

3. Create a .env file in the project root directory with the following variables:

```bash
FLASK_APP=app.py
FLASK_ENV=development
MONGO_URI=mongodb://localhost:27017/stocktradingapp
SECRET_KEY=your_secret_key
JWT_SECRET_KEY=your_jwt_secret_key
SMTP_HOST=your_smtp_host
SMTP_PORT=your_smtp_port
SMTP_USERNAME=your_smtp_username
SMTP_PASSWORD=your_smtp_password
```

4. Activate the Pipenv shell and run the Flask app:
```bash
pipenv shell
flask run
```

### Docker Setup (optional)
If you prefer to use Docker, you can build and run the Docker image:
    
```bash
docker-compose up --build
```

### Seeding the Database
You can use the seed script to populate the database with some initial data:
```bash
python seed.py
```

This will create 50 users and 50 stocks with random data.

### API Documentation
Swagger UI is set up for interactive API documentation. You can access it at
http://localhost:5000/swagger

### Contributing
To contribute to the Stock Trading App, follow these steps:

- Fork the repository.
- Create a new branch: git checkout -b <branch_name>.
- Make your changes and commit them: git commit -m '<commit_message>'.
- Push to the original branch: git push origin <project_name>/<location>.
- Create the pull request.


### Contact
If you want to contact me you can reach me at vivek.keshore@senecaglobal.com