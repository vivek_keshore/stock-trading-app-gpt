import os
from faker import Faker
from pymongo import MongoClient
from dotenv import load_dotenv

load_dotenv()

# Connect to MongoDB
mongo = MongoClient(os.getenv('MONGO_URI'))


client = MongoClient(os.environ['MONGO_URI'])
db = client.get_database()
user_collection = db['user']
stock_collection = db['stock']

# Generate fake data
fake = Faker()

# Insert 50 fake users
for _ in range(50):
    user_collection.insert_one({
        'name': fake.name(),
        'email': fake.email(),
        'password': 'test1234',
        'opening_balance': 10000,
    })

# Insert 50 fake stocks
for _ in range(50):
    stock_collection.insert_one({
        'name': fake.company(),
        'price': fake.random_int(min=10, max=1000, step=1),
        'status': fake.random_element(elements=('positive', 'negative'))
    })

print("Database seeded!")
